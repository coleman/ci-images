
variable "builder_user" {
  default = "builder"
}

source "digitalocean" "ubuntu_20_04" {
  # DIGITALOCEAN_API_TOKEN is taken as the default
  #api_token = ""
  image = "ubuntu-20-04-x64"
  region = "nyc3"
  size = "1GB"
  snapshot_name = "redox-base-image-${timestamp()}"
  ssh_username = "root"
}


build {
  sources = [
    "source.digitalocean.ubuntu_20_04"
  ]
  provisioner "shell" {
    scripts = [
      "scripts/redox.ubuntu.sh",
    ]
  }
  provisioner "shell" {
    inline = [
      # -m creates a home directory
      "useradd -m ${var.builder_user}",
    ]
  }
  provisioner "shell" {
    # this execute_command wraps the scripts block in a command that changes
    # to our user first
    execute_command = "chmod +x {{ .Path }}; {{ .Vars }} sudo -u ${var.builder_user} {{ .Path }}"
    scripts = [
      "scripts/redox.builder_user.sh",
    ]
  }
}

