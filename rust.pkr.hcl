

source "digitalocean" "fedora_32" {
  # DIGITALOCEAN_API_TOKEN is taken as the default
  #api_token = ""
  image = "fedora-32-x64"
  region = "nyc3"
  size = "1GB"
  snapshot_name = "rust-ci-${timestamp()}"
  ssh_username = "root"
}


build {
  sources = [
    "source.digitalocean.fedora_32"
  ]
  provisioner "shell" {
    scripts = [
      "scripts/rust.fedora.sh"
    ]
  }
}
