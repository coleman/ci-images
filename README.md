# ci-images

Base images that can be used for CI.

Packer 1.5 or greater is required.


## Digitalocean VM Images

Currently we build a VM image on Digitalocean. This is for use with the Drone CI
[Digitalocean runner](https://docs.drone.io/pipeline/digitalocean/syntax/cloning/).
Pre-baking an image lets us speed up CI runs. Specifically, we can

* check out the majority of the code ahead of time (this takes a long time)
* pre-install dependencies

We **must** use custom git clone logic to take advantage of our pre-baked image.
Otherwise, the Drone CI runner will do a fresh checkout in a temporary directory.

The VM image is destroyed after the CI job is done.

You must set `DIGITALOCEAN_API_TOKEN` in your shell.

List all your images with the `doctl` command line tool like this.

```
doctl compute image list
```

Clean up old snapshots. Snapshots are pretty cheap, but not free.

## Usage

```
packer build redox.pkr.hcl
```

This build will take around 20 minutes. If it's successful, an output ID will
be printed to the console, and visible in your Digital Ocean web UI, as well as
the `doctl` cli.

