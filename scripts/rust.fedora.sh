#!/bin/bash

dnf update -y

dnf install -y \
    gcc \
    git \
    wget \
    unzip \
    openssl-devel

useradd builder  # todo: use this 

curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly

echo 'source $HOME/.cargo/env' > /etc/profile.d/cargo-env.sh
