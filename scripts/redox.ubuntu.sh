#!/bin/bash

set -e

apt-get update -y

apt-get install -y \
    gcc \
   	autoconf \
   	autopoint \
   	bison \
   	build-essential \
   	cmake \
   	curl \
   	file \
   	flex \
   	fuse \
   	genisoimage \
   	git \
   	gperf \
   	libc6-dev-i386 \
   	libfuse-dev \
   	libhtml-parser-perl \
   	libpng-dev \
   	libtool \
   	m4 \
   	nasm \
   	pkg-config \
   	syslinux-utils \
   	texinfo \
    qemu-system-x86

