#!/bin/bash

echo "RUNNING AS $(whoami)"

# Note: probably no KVM on DO
cd /home/builder
curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly
source /home/builder/.cargo/env && cargo install xargo cargo-config

# clone with https to avoid ssh config on CI
git clone https://gitlab.redox-os.org/redox-os/redox.git --origin origin --recursive

